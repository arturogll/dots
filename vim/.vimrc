call plug#begin('~/.vim/plugged')
" Temas
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'morhetz/gruvbox'
Plug 'joshdick/onedark.vim'
" IDE
" Use release branch (Recommend)
Plug 'mattn/emmet-vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'sheerun/vim-polyglot'
Plug 'scrooloose/nerdcommenter'
Plug 'terryma/vim-multiple-cursors'
Plug 'w0rp/ale'
Plug 'prettier/vim-prettier', { 'do': 'npm install' }
Plug 'christoomey/vim-tmux-navigator'
Plug 'itchyny/lightline.vim'
Plug 'scrooloose/nerdtree'
Plug 'easymotion/vim-easymotion'
call plug#end()
let g:lightline = {
      \ 'colorscheme': 'onedark',
      \ }


syntax on
colorscheme onedark
set laststatus=2
" Enable true color 启用终端24位色
if exists('+termguicolors')
	let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
	let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
	set termguicolors
endif
set number
set numberwidth=1
set cursorline
set ruler
" set encoding=utf-8
set sw=2
set softtabstop=0 noexpandtab
set tabstop=2
set laststatus=2
if $TMUX == ''
	set clipboard+=unnamed
endif

hi LineNr guibg=#2c3038
hi LineNr guifg=#636d80
let mapleader=" "
let NERDTreeQuitOnOpen=1
let g:NERDSpaceDelims = 1
map <Leader>nt :NERDTreeToggle<CR>
map <Leader>s <Plug>(easymotion-s2)
" lintern syntax js
let g:ale_fixers = {'javascript': ['prettier_standard']}
let g:ale_linters = {'javascript': ['']}
let g:ale_fix_on_save = 1
vnoremap <C-c> "+y :let @+=@*<CR>
